<?php

/**
 * Part of AuthRemoteToken plugin, model for TokenKey
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2024 Denis Chenu <http://sondages.pro>
 * @license AGPL v3
 * @version 0.3.4
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */


namespace AuthRemoteToken\models;

use App;
use CDbCriteria;
use CActiveDataProvider;
use CSort;
use Permission;
use User;

class TokenKey extends \LSActiveRecord
{
/**
 * Class AuthRemoteToken\models\TokenKey
 *
 * @property integre $id : primary key
 * @property string $token : the token as pripary key
 * @property integer $uid : the user
 * @property text $comment : the comment
 * @property string $passkey : the key (hashed in save)
 * @property date $created :datetime for epriration
 * @property date $expiration :datetime for epriration
*/

    /**
     * Init and set default
     **/
    public function init()
    {
        $this->created = dateShift(date("Y-m-d H:i:s"), "Y-m-d H:i:s", App()->getConfig("timeadjust"));
    }

    /**
     * Create a new token ley
     * @param integer $uid user id
     **/
    public static function create($uid)
    {
        $tokenkey = new self;
        $tokenkey->uid = $uid;
        $tokenkey->created = dateShift(date("Y-m-d H:i:s"), "Y-m-d H:i:s", App()->getConfig("timeadjust"));
        $tokenkey->generateToken();
        $tokenkey->generatePasskey();
        return $tokenkey;
    }

    /** @inheritdoc */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /** @inheritdoc */
    public function tableName()
    {
        return '{{authremotetoken_tokenkey}}';
    }

    /** @inheritdoc */
    public function primaryKey()
    {
        return 'id';
    }

    /**
     * SGet the token key by token
     * @param string $token
     * @return self
     **/
    public function findByToken($token)
    {
        return self::model()->find("token = :token", array(":token" => $token));
    }

    /** @inheritdoc */
    public function rules()
    {
        return array(
            array('id', 'numerical', 'integerOnly' => true, 'min' => 1),
            array('id', 'unique'),
            array('token', 'length', 'min' => 12, 'max' => 100),
            array('token', 'unique'),
            // @todo : difference with user_name
            array('uid', 'numerical', 'integerOnly' => true, 'min' => 1),
            array('comment', 'safe'),
            /*array('passkey', 'length', 'min' => 12, 'max'=>32),*/
        );
    }

    /**
     * @inheritDoc
     * @return array
     */
    public function getColumns()
    {
        $columns = array(
            'buttons' => array(
                'header' => gT('Action'),
                'class' => 'bootstrap.widgets.TbButtonColumn',
                'template' => '{edittoken}', // '{edittoken}{deletetoken}',
                'buttons' => $this->getButtons(),
            ),
            'comment' => array(
                 "name" => 'comment',
                 "header" => gT("Comment"),
            ),
            'token' => array(
                 "name" => 'token',
                 "header" => gT("Token"),
            ),
            'created' => array(
                 "name" => 'created',
                 "header" => gT("Date created"),
                 'filter' => false,
            ),
            'lastused' => array(
                 "name" => 'lastused',
                 "header" => gT("Last time used"),
                 'filter' => false,
            ),
            'expiration' => array(
                 "name" => 'expiration',
                 "header" => gT("Date expire"),
                 'filter' => false,
            ),
        );
        return $columns;
    }

    /**
     * Genereate a random token
     * Compare with existing token to disallow same vakue
     */
    private function generateToken()
    {
        $nbcar = \AuthRemoteToken\Utilities::TOKENLENGHT;
        $count = 0;
        while ($count < 100) {
            $newtoken = App()->securityManager->generateRandomString($nbcar);
            $exist = self::model()->count("token = :token", [':token' => $newtoken]);
            $username = User::model()->count("users_name = :token", [':token' => $newtoken]);
            if (!$exist && !$username) {
                $this->token = $newtoken;
                return;
            }
            $count++;
        }
        throw new CHttpException(500, "Unable to create a token of $nbcar after $count attempts");
    }

    /**
     * Generate a random password
     */
    private function generatePasskey()
    {
        $this->passkey = App()->securityManager->generateRandomString(\AuthRemoteToken\Utilities::PASSKEYLENGHT);
    }

    /**
     * Save passkey
     */
    public function setPassKey($passkey)
    {
        $this->passkey = password_hash($passkey, PASSWORD_DEFAULT);
    }

    /**
     * Check passkey
     *
     * @param string $sPassword The clear password
     * @return boolean
     */
    public function checkPassKey($passkey)
    {
        if (empty($this->passkey)) {
            return false;
        }
        // Password is OK
        if (password_verify($passkey, $this->passkey)) {
            return true;
        }
        return false;
    }

    /**
     * @return CActiveDataProvider
     */
    public function search()
    {
        $this->setScenario('search');
        $criteria = new CDbCriteria();
        $criteria->compare('uid', \AuthRemoteToken\Utilities::getUserId());
        $criteria->compare('comment', $this->comment, true);
        $criteria->compare('token', $this->token, true);

        $sort = $this->getSort();
        $dataProvider = new CActiveDataProvider('\AuthRemoteToken\models\TokenKey', array(
            'sort' => $sort,
            'criteria' => $criteria,
        ));
        return $dataProvider;
    }

    /**
     * Return the buttons columns
     * @see https://www.yiiframework.com/doc/api/1.1/CButtonColumn
     * @return array
     */
    public function getButtons()
    {
        $gridButtons['edittoken'] = array(
            'label' => '<span class="sr-only">' . gT("Edit") . '</span><span class="fa fa-edit" aria-hidden="true"></span>',
            'imageUrl' => false,
            'url' => 'App()->createUrl("admin/pluginhelper",array("plugin"=>"AuthRemoteToken","sa"=>"fullpagewrapper","method"=>"actionEdit","token"=>$data->token));',
            'options' => array(
                'class' => "btn btn-default btn-edittoken",
                'data-toggle' => "tooltip",
                'title' => gT("Edit")
            ),
        );
        $gridButtons['deletetoken'] = array(
            'label' => '<span class="sr-only">' . gT("Delete") . '</span><span class="text-warning fa fa-trash" aria-hidden="true"></span>',
            'imageUrl' => false,
            'url' => 'App()->createUrl("admin/pluginhelper",array("plugin"=>"AuthRemoteToken","sa"=>"fullpagewrapper","method"=>"actionDelete","token"=>$data->token));',
            'options' => array(
                'class' => "btn btn-default btn-deletetoken",
                'data-toggle' => "tooltip",
                'title' => gT("Delete this remote token")
            ),
            'click' => 'function(event){ window.LS.gridButton.confirmGridAction(event,$(this)); }',
        );
        return $gridButtons;
    }

    public function getSort()
    {
        $sort = new CSort();
        $sort->defaultOrder = App()->db->quoteColumnName($this->tableAlias . '.token') . ' ASC';
        $sort->multiSort = true;
        $sort->attributes = array(
            'id',
            'comment',
            'token',
            'created',
            'expiration'
        );
    }
}
