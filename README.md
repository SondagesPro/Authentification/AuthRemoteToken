# AuthRemoteToken

LimeSurvey Authentication via random Access Token key and random password only for Remote Control.

This plugin allow specific user to create Access Token key for remoteControl API. Can be used when authentication uses a non-compatible system (CAS, SAML …).

## Usage 

### Administration

As administrator, after plugin activation, you have too :

- Activate [JSON RPC in LimeSurvey](https://manual.limesurvey.org/RemoteControl_2_API)
- Set the maximum delay of Access Token and Password (in plugin setting)
- Allow user to authenticate via Access Token Key (in user management)

### Using for remote control

- If user is allowed to use Access Token key : there is a new menu item in user menu
- User can manage multiple access token
- Token Key and Password are randomly set when create
- Token password is encrypted, the user must keep it himself, there are no way to update it.
- The URL to be used for RemoteControl connection are shown in Token Key manager
- Token Key must be used as username and Token Password as password when using [get_session_key](https://manual.limesurvey.org/RemoteControl_2_API#get_session_key)

## Contribute and issue

Contribution are welcome, for patch and issue : use [gitlab](https://gitlab.com/SondagesPro/Authentification/AuthRemoteToken).

## Home page & Copyright

- HomePage <http://extensions.sondages.pro/>
- Copyright © 2022 Denis Chenu <https://www.sondages.pro>
- Copyright © 2022 OECD <https://www.oecd.org/>
- Licence : GNU Affero General Public License <https://www.gnu.org/licenses/agpl-3.0.html>
- [Donate](https://support.sondages.pro/open.php?topicId=12), [Liberapay](https://liberapay.com/SondagesPro/), [OpenCollective](https://opencollective.com/sondagespro) 
