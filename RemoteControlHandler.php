<?php

/**
 * Handler for extendRemoteControl Plugin for LimeSurvey : add yours functions here
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2017 Denis Chenu <http://sondages.pro>
 * @license AGPL v3
 * @version 0.1.0-beta1
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

class RemoteControlHandler extends remotecontrol_handle
{
    /* var \LSUserIdentity */
    protected $identity;

    /**
     * @inheritdoc
     * fix issue in LimeSurvey core
     * @see https://bugs.limesurvey.org/view.php?id=18177
     */
    public function get_session_key($username, $password, $plugin = 'AuthRemoteToken')
    {
        $username = (string) $username;
        $password = (string) $password;
        $loginResult = $this->doLogin($username, $password, $plugin);
        if ($loginResult) {
            /* Plugin MUST set identify->user, keep if for API in case */
            if (!empty($this->identify->user->users_name)) {
                $username = $this->identify->user->users_name;
            }
            $this->fillSession($username);
            $sSessionKey = Yii::app()->securityManager->generateRandomString(32);
            $session = new Session();
            $session->id = $sSessionKey;
            $session->expire = time() + (int) Yii::app()->getConfig('iSessionExpirationTime', ini_get('session.gc_maxlifetime'));
            $session->data = $username;
            $session->save();
            return $sSessionKey;
        }
        if (!empty($this->identity->errorMessage)) {
            return array('status' => $this->identity->errorMessage);
        }
        return array('status' => 'Invalid user name or password');
    }

    /**
     * @see parent _doLogin
     * Set $this->identity
     * @return boolean, set identify
     */
    protected function doLogin($sUsername, $sPassword, $sPlugin)
    {
        /* @var $identity LSUserIdentity */
        $identity = new \LSUserIdentity($sUsername, $sPassword);
        $identity->setPlugin($sPlugin);
        $event = new PluginEvent('remoteControlLogin');
        $event->set('identity', $identity);
        $event->set('plugin', $sPlugin);
        $event->set('username', $sUsername);
        $event->set('password', $sPassword);
        App()->getPluginManager()->dispatchEvent(
            $event,
            array($sPlugin)
        );
        $authenticate = $identity->authenticate();
        $this->identify = $identity;
        return boolval($authenticate);
    }

    /**
     * @see parent _jumpStartSession
     * Use $this->identity
     * @return boolean
     */
    protected function fillSession($username = null)
    {
        /* Plugin MUST set identify->user, keep if for API in case */
        if (!empty($identity->user)) {
            $aUserData = $identity->user->attributes;
        } else {
            $aUserData = \User::model()->findByAttributes(
                array('users_name' => (string) $username)
            )->attributes;
        }

        $session = array(
            'loginID' => intval($aUserData['uid']),
            'user' => $aUserData['users_name'],
            'full_name' => $aUserData['full_name'],
            'htmleditormode' => $aUserData['htmleditormode'],
            'templateeditormode' => $aUserData['templateeditormode'],
            'questionselectormode' => $aUserData['questionselectormode'],
            'dateformat' => $aUserData['dateformat'],
            'adminlang' => 'en'
        );
        foreach ($session as $k => $v) {
            Yii::app()->session[$k] = $v;
        }
        Yii::app()->user->setId($aUserData['uid']);
        return true;
    }
}
