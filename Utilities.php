<?php

/**
 * Some Utilities
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2022 Denis Chenu <http://www.sondages.pro>
 * @license AGPL v3
 * @version 0.1.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

namespace AuthRemoteToken;

use App;
use Yii;
use Permission;

class Utilities
{
    /** @var integer TOKENLENGHT */
    const TOKENLENGHT = 12;
    /** @var integer PASSKEYLENGHT */
    const PASSKEYLENGHT = 32;

     /**
     * Translate by this plugin
     * @see reloadAnyResponse->_setConfig
     * @param string $string to translate
     * @param string $language for translation
     * @return string
     */
    public static function translate($string, $language = null)
    {
        return Yii::t('', $string, array(), 'AuthRemoteTokenLang', $language);
    }

     /**
     * return the current user id
     * to replace same function for static usage an compatibility
     * @see https://github.com/LimeSurvey/LimeSurvey/commit/7008e67ce49b7dba62d22efff0ea5f10435426a6
     * @return null|integer
     */
    public static function getUserId()
    {
        if (version_compare(App()->getConfig('versionnumber'), '4.4.0-RC2', '<')) {
            return Permission::getUserId();
        }
        return Permission::model()->getUserId();
    }
}
