<?php

/**
 * Authentification via random Access Token key and random password only for Remote Control
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2022 Denis Chenu <http://sondages.pro>
 * @copyright 2022 OECD <http://jevaluemaformation.com>
 * @license AGPL v3
 * @version 0.3.2
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

class AuthRemoteToken extends AuthPluginBase
{
    protected $storage = 'DbStorage';

    const DBVERSION = 1;

    protected static $description = 'Authentification via random Access Token key and random password only for Remote Control';
    protected static $name = 'AuthRemoteToken';

    /** @inheritdoc, this plugin have this public method */
    public $allowedPublicMethods = array(
        'actionList',
        'actionCreate',
        'actionEdit',
        'actionSave',
        'actionDelete'
    );

    protected $settings = array (
        'maxTimeDelay' => array(
            'type' => 'string',
            'label' => 'The max time when create a new token key. User can update it but not after this minimal delay.',
            'help' => 'If integer : in day',
            'default' => '',
            'htmlOptions' => array(
                'placeholder' => '180 (days)'
            ),
        ),
        'replaceDefaultLink' => array(
            'type' => 'boolean',
            'label' => 'Replace default remotecontrol link, try to connect with this plugin before leave default',
            'help' => 'If username didnt exist in core LimeSurvey username and exist in this plugin username : use this plugin for Authentication',
            'default' => 0,
        ),
        'allowUpdateToken' => array(
            'type' => 'boolean',
            'label' => 'Allow user to set the token key.',
            'help' => 'User can set the token key. Token Password still random',
            'default' => 0,
        ),
    );

    /* @var boolean force authentication by current plugin */
    private $isForced = false;
    /* @var string|null : token to be used if forced */
    private $token;
    /* @var string|null : passkey to be used if forced */
    private $passkey;

    public function init()
    {
        /* The login action */
        $this->subscribe('remoteControlLogin');
        $this->subscribe('newUserSession');
        /* Permission */
        $this->subscribe('getGlobalBasePermissions');
        /* Alias (and other) */
        $this->subscribe('afterPluginLoad');

        /* Create the DB */
        $this->subscribe('beforeActivate');
        /* New connexion type */
        $this->subscribe('newUnsecureRequest');
        /* replace default connexion */
        if ($this->get('replaceDefaultLink')) {
            $this->subscribe('beforeControllerAction');
        }
    }


    public function beforeActivate()
    {
        $oEvent = $this->getEvent();
        if (Yii::app()->getConfig("RPCInterface") != 'json') {
            $oEvent->set('success', false);
            $oEvent->set('message', "Only for json RPC remote control.");
            return;
        }
        $oEvent->set('success', $this->setDb());
    }

    /**
     * Add AuthLDAP Permission to global Permission
     * @return void
     */
    public function getGlobalBasePermissions()
    {
        $this->getEvent()->append('globalBasePermissions', array(
            'auth_remotetoken' => array(
                'create' => false,
                'update' => false,
                'delete' => false,
                'import' => false,
                'export' => false,
                'title' => gT("Allow to create remote token authentication"),
                'description' => gT("Remote token authentication"),
                'img' => ' fa fa-key'
            ),
        ));
    }

    public function actionList()
    {
        if (Yii::app()->getConfig("RPCInterface") != 'json') {
            throw new CHttpException(403, $this->gT("JSON RPC is not activated."));
        }
        if (!Permission::model()->hasGlobalPermission('auth_remotetoken', 'read')) {
            throw new CHttpException(403, $this->gT("You are not allowed to manage remote token key"));
        }
        $TokenKey = \AuthRemoteToken\models\TokenKey::model();
        $TokenKey->setScenario('search');
        $TokenKey->setAttributes(App()->getRequest()->getParam('AuthRemoteToken_models_TokenKey'));
        $render = $this->renderPartial(
            "manage.list",
            array(
                'model' => $TokenKey,
                'settings' => $this->getPluginSettings(true),
            ),
            1
        );
        return $render;
    }

    public function actionCreate()
    {
        return $this->actionEdit();
    }

    public function actionEdit()
    {
        if (Yii::app()->getConfig("RPCInterface") != 'json') {
            throw new CHttpException(403, $this->gT("JSON RPC is not activated."));
        }
        if (!Permission::model()->hasGlobalPermission('auth_remotetoken', 'read')) {
            throw new CHttpException(403, $this->gT("You are not allowed to manage remote token key"));
        }
        $token = App()->getRequest()->getParam('token');
        $isNew = false;
        if (!empty($token)) {
            $title = $this->gT("Edit token key");
            $TokenKey = \AuthRemoteToken\models\TokenKey::model()->findByToken($token);
            if (empty($TokenKey) || $TokenKey->uid != \AuthRemoteToken\Utilities::getUserId()) {
                throw new CHttpException(403, $this->gT("You are not allowed to manage this remote token key"));
            }
        } else {
            $title = $this->gT("Create a new token key");
            $TokenKey = \AuthRemoteToken\models\TokenKey::create(\AuthRemoteToken\Utilities::getUserId());
            $isNew = true;
        }
        $settings = array();
        $settings['token'] = array(
            'type' => 'string',
            'label' => $this->gT('Personal Access Token Key'),
            'help' => $this->gT("Replace \$username in get_session_key function."),
            'current' => $TokenKey->token,
            'htmlOptions' => array(
                'readonly' => (!$this->get('allowUpdateToken') || !$isNew),
                'class' => 'copy-help',
                'minlength' => 12,
                'maxlength' => 100,
            )
        );
        if (empty($TokenKey->id)) {
            $settings['passkey'] = array(
                'type' => 'string',
                'label' => $this->gT('Personal Access Token Password'),
                'current' => $TokenKey->passkey,
                'htmlOptions' => array(
                    'readonly' => 1,
                    'class' => 'copy-help'
                ),
                'help' => "<div>" .
                    $this->gT("Replace \$password in get_session_key function.") .
                    "</div>" .
                    "<div class='text-danger'><strong>" .
                    $this->gT("Make sure you save it - you won't be able to access it again.") .
                    "</strong></div>",
            );
        }
        $settings['comment'] = array(
            'type' => 'text',
            'label' => $this->gT('Comment'),
            'current' => $TokenKey->comment,
            //'help' => $this->gT("Replace \$username in get_session_key function."),
        );
        $expiration = $TokenKey->expiration;
        $maxTimeDelay = $this->get('maxTimeDelay');
        if (empty($maxTimeDelay)) {
            $maxTimeDelay = 180;
        }
        if (empty($expiration)) {
            $expiration = date('Y-m-d H:i:s', strtotime("+{$maxTimeDelay} days"));
        }
        $datetimeobj = DateTime::createFromFormat("Y-m-d H:i:s", $expiration);
        if ($datetimeobj == null) { //MSSQL uses microseconds by default in any datetime object
            $datetimeobj = DateTime::createFromFormat("Y-m-d H:i:s.u", $expiration);
        }
        $dateformatdetails = getDateFormatData(Yii::app()->session['dateformat']);
        if ($datetimeobj) {
            $expiration = $datetimeobj->format($dateformatdetails['phpdate'] . " H:i");
        }
        $settings['expiration'] = array(
            'type' => 'date',
            'label' => $this->gT('Expiration Date'),
            'current' => $expiration,
            'help' => sprintf($this->gT("Maximum days for your access token key are %s. You can update here at any time."), $maxTimeDelay)
        );
        $render = $this->renderPartial(
            "manage.edit",
            array(
                'title' => $title,
                'model' => $TokenKey,
                'settings' => $settings,
                'token' => $token
            ),
            1
        );
        return $render;
    }

    public function actionSave()
    {
        if (!Permission::model()->hasGlobalPermission('auth_remotetoken', 'read')) {
            throw new CHttpException(403, $this->gT("You are not allowed to manage remote token key"));
        }
        if (!App()->getRequest()->isPostRequest) {
            throw new CHttpException(400, $this->gT("Bad request"));
        }
        $token = App()->getRequest()->getQuery('token');
        if (!empty($token)) {
            $TokenKey = \AuthRemoteToken\models\TokenKey::model()->findByToken($token);
            if (empty($TokenKey) || $TokenKey->uid != \AuthRemoteToken\Utilities::getUserId()) {
                throw new CHttpException(403, $this->gT("You are not allowed to manage this remote token key"));
            }
            if (App()->getRequest()->getPost('deleteAuthRemoteToken') == 'delete') {
                $this->actionDelete();
            }
        } else {
            $token = App()->getRequest()->getPost('token');
            if (empty($token)) {
                throw new CHttpException(400, $this->gT("Bad request, no token"));
            }
            $passkey = App()->getRequest()->getPost('passkey');
            if (empty($passkey)) {
                throw new CHttpException(400, $this->gT("Bad request, no passkey"));
            }
            $TokenKey = new \AuthRemoteToken\models\TokenKey();
            $TokenKey->token = $token;
            $TokenKey->setPassKey($passkey);
            $TokenKey->uid = \AuthRemoteToken\Utilities::getUserId();
        }
        $TokenKey->comment = App()->getRequest()->getPost('comment');
        $expiration = App()->getRequest()->getPost('expiration');
        $dateformatdetails = getDateFormatData(Yii::app()->session['dateformat']);
        $datetimeobj = DateTime::createFromFormat('!' . $dateformatdetails['phpdate'], $expiration);
        $maxTimeDelay = $this->get('maxTimeDelay');
        if (empty($maxTimeDelay)) {
            $maxTimeDelay = 180;
        }
        $maxdate = date('Y-m-d H:i', strtotime("+{$maxTimeDelay} days"));
        if ($datetimeobj) {
            $expiration = $datetimeobj->format('Y-m-d H:i');
            if ($expiration > $maxdate) {
                $expiration = $maxdate;
            }
        } else {
            $expiration = $maxdate;
        }
        $TokenKey->expiration = $expiration;
        if (!$TokenKey->save()) {
            App()->setFlashMessage(
                CHtml::errorSummary(
                    $TokenKey,
                    $this->gT("Unable to save personal access token.")
                ),
                'error'
            );
            $redirectUrl = App()->createUrl(
                'admin/pluginhelper/sa/fullpagewrapper',
                array(
                    'plugin' => get_class($this),
                    'method' => 'actionCreate'
                )
            );
            App()->getRequest()->redirect($redirectUrl, true, 303);
        }
        App()->setFlashMessage($this->gT("Save personal access token with success."));
        if (App()->getRequest()->getPost('save' . get_class($this)) == 'redirect') {
            $redirectUrl = App()->createUrl(
                'admin/pluginhelper/sa/fullpagewrapper',
                array(
                    'plugin' => get_class($this),
                    'method' => 'actionList'
                )
            );
            App()->getRequest()->redirect($redirectUrl);
        }
        $redirectUrl = App()->createUrl(
            'admin/pluginhelper/sa/fullpagewrapper',
            array(
                'plugin' => get_class($this),
                'method' => 'actionEdit',
                'token' => $TokenKey->token
            )
        );
        App()->getRequest()->redirect($redirectUrl, true, 303);
    }

    /**
     * Delete token
     */
    public function actionDelete()
    {
        if (!Permission::model()->hasGlobalPermission('auth_remotetoken', 'read')) {
            throw new CHttpException(403, $this->gT("You are not allowed to manage remote token key"));
        }
        if (!App()->getRequest()->isPostRequest) {
            throw new CHttpException(400, $this->gT("Bad request"));
        }
        $token = App()->getRequest()->getParam('token');
        if (empty($token)) {
            throw new CHttpException(400, $this->gT("Bad request, no token"));
        }
        $TokenKey = \AuthRemoteToken\models\TokenKey::model()->findByToken($token);
        if (empty($TokenKey) || $TokenKey->uid != \AuthRemoteToken\Utilities::getUserId()) {
            throw new CHttpException(403, $this->gT("You are not allowed to manage this remote token key"));
        }
        $TokenKey->delete();
        $redirectUrl = App()->createUrl(
            'admin/pluginhelper/sa/fullpagewrapper',
            array(
                'plugin' => get_class($this),
                'method' => 'actionList'
            )
        );
        App()->getRequest()->redirect($redirectUrl);
    }

    /**
     * Add the menu
     * @retun void
     */
    public function beforeAdminMenuRender()
    {
        $menuEvent = $this->getEvent();
        if (Yii::app()->getConfig("RPCInterface") != 'json') {
            return;
        }
        if (!Permission::model()->hasGlobalPermission('auth_remotetoken', 'read')) {
            return;
        }
        /* Usage of script : more clear in user menu */
        $url = App()->getController()->createUrl(
            'admin/pluginhelper',
            array('plugin' => $this->getName(), 'sa' => 'fullpagewrapper','method' => 'actionList' )
        );
        $menuitem = $this->renderPartial("menuitem", array('url' => $url), 1);
        $script = "if (!$('#AuthRemoteToken-link').length) {\n"
            . "\t$('.navbar .icon-user').closest('.dropdown').find('.dropdown-menu .divider').before('" . trim(json_encode($menuitem), '"') . "');\n"
            . "}\n";
        App()->clientScript->registerScript('AuthRemoteTokenLink', $script, \CClientScript::POS_READY);
    }

    /**
    * Create needed DB
    * @return void
    */
    private function setDb()
    {
        if (intval($this->get("dbVersion")) >= self::DBVERSION) {
            return true;
        }
        /* dbVersion not needed */
        if (!$this->api->tableExists($this, 'tokenkey')) {
            $this->api->createTable($this, 'tokenkey', array(
                'id' => 'pk',
                'token' => 'string(150) not NULL',
                'uid' => 'int not NULL',
                'comment' => 'text',
                'passkey' => 'text',
                'created' => 'datetime',
                'lastused' => 'datetime NULL',
                'expiration' => 'datetime NULL',
            ));
            $tableName = $this->api->getTable($this, 'tokenkey')->tableName();
            App()->getDb()->createCommand()->createIndex('tokenkey_token', $tableName, 'token', true);
            $this->set("dbVersion", 1);
        }

        /* all done */
        $this->set("dbVersion", self::DBVERSION);
        return true;
    }

    /**
    * Add needed alias and put it in autoloader
    * @return void
    */
    public function afterPluginLoad()
    {
        Yii::setPathOfAlias('AuthRemoteToken', dirname(__FILE__));
        $messageSource = array(
            'class' => 'CGettextMessageSource',
            'cacheID' => 'AuthRemoteTokenLanguage',
            'cachingDuration' => 3600,
            'forceTranslation' => true,
            'useMoFile' => true,
            'basePath' => __DIR__ . DIRECTORY_SEPARATOR . 'locale',
            'catalog' => 'messages',// default from Yii
        );
        Yii::app()->setComponent('AuthRemoteTokenLang', $messageSource);
        /* Add the menu (workaround) */
        $this->subscribe('beforeAdminMenuRender');
    }

    /**
     * Set username and password by event
     *
     * @return null
     */
    public function remoteControlLogin()
    {
        $event = $this->getEvent();
        $this->setUsername($event->get('username'));
        $this->setPassword($event->get('password'));
    }

    public function newUserSession()
    {
        $userSessionEvent = $this->getEvent();
        $identity = $userSessionEvent->get('identity');
        if ($identity->plugin != 'AuthRemoteToken' && !$this->isForced) {
            return;
        }
        if ($this->isForced) {
            $token = $this->token;
            $passkey = $this->passkey;
        } else {
            $token = $this->getUsername();
            $passkey = $this->getPassword();
        }
        $TokenKey = \AuthRemoteToken\models\TokenKey::model()->findByToken($token);
        if (empty($TokenKey)) {
            $this->setAuthFailure(self::ERROR_USERNAME_INVALID, '', $userSessionEvent);
            return;
        }

        if (empty($TokenKey->expiration)) {
            $this->setAuthFailure(self::ERROR_USERNAME_INVALID, '', $userSessionEvent);
            return;
        }
        $expiration = strtotime($TokenKey->expiration);
        $now = strtotime("now");
        if ($expiration < $now) {
            $this->setAuthFailure(self::ERROR_USERNAME_INVALID, '', $userSessionEvent);
            return;
        }
        if (!$TokenKey->checkPassKey($passkey)) {
            $this->setAuthFailure(self::ERROR_USERNAME_INVALID, '', $userSessionEvent);
            return;
        }
        if (!Permission::model()->hasGlobalPermission('auth_remotetoken', 'read', $TokenKey->uid)) {
            $this->setAuthFailure(self::ERROR_USERNAME_INVALID, '', $userSessionEvent);
            return;
        }
        $user = User::model()->findByPk($TokenKey->uid);
        if (empty($user)) {
            \Yii::log("Invalid token $token", 'plugin.AuthRemoteToken.newUserSession.AuthFailure', \CLogger::LEVEL_INFO);
            $this->setAuthFailure(self::ERROR_USERNAME_INVALID, '', $userSessionEvent);
        }
        $this->setUsername($user->users_name);
        \Yii::log("User : {$user->users_name}  log with token $token", 'plugin.AuthRemoteToken.newUserSession.AuthSuccess', \CLogger::LEVEL_INFO);
        $TokenKey->lastused = date('Y-m-d H:i:s');
        $TokenKey->save(['lastused']);
        $this->setAuthSuccess($user, $userSessionEvent);
    }

    /**
     * @inheritoc
     * Replace to use own event
     * @see https://bugs.limesurvey.org/view.php?id=17654
     *
     * @param User $user
     * @param PluginEvent $event
     * @return AuthPluginBase
     */
    public function setAuthSuccess(User $user, \LimeSurvey\PluginManager\PluginEvent $event = null)
    {
        if (empty($event)) {
            $event = $this->getEvent();
        }
        $identity = $event->get('identity');
        $identity->id = $user->uid;
        $identity->username = $user->users_name;
        $identity->user = $user;
        $event->set('identity', $identity);
        $event->set('result', new LSAuthResult(self::ERROR_NONE));
        return $this;
    }

    /**
     * @inheritoc
     * Replace to use own event
     * @see https://bugs.limesurvey.org/view.php?id=17654
     *
     * @param int $code Any of the constants defined in this class
     * @param string $message An optional message to return about the failure
     * @return AuthPluginBase
     */
    public function setAuthFailure($code = self::ERROR_UNKNOWN_IDENTITY, $message = '', \LimeSurvey\PluginManager\PluginEvent $event = null)
    {
        if (empty($event)) {
            $event = $this->getEvent();
        }
        $identity = $event->get('identity');
        $identity->id = null;
        $event->set('result', new LSAuthResult($code, $message));

        return $this;
    }

    /**
     * Login system for remote control, allow to update username
     * @todo
     */
    public function newUnsecureRequest()
    {
        $RequestEvent = $this->getEvent();
        if ($RequestEvent->get('target') != $this->getName()) {
            return;
        }
        $RPCType = Yii::app()->getConfig("RPCInterface");
        if ($RPCType != 'json') {
            header("Content-type: application/json");
            echo json_encode(array(
                'status' => 'error',
                'message' => 'Only for json RPCInterface',
            ));
            Yii::app()->end();
        }
        if (App()->request->getIsPostRequest()) {
            $this->setRemoteHandler();
        }
    }

    /**
     * @see event
     * check if it's remote control and get_session_key funtcion, if user don't exist but access key exist : use own RemoteHandler
     */
    public function beforeControllerAction()
    {
        $event = $this->getEvent();
        if ($event->get('controller') != 'admin' && $event->get('action') != 'remotecontrol') {
            return;
        }
        if (!App()->request->getIsPostRequest()) {
            return;
        }
        $request = @json_decode(file_get_contents('php://input'), true);
        if (!isset($request['method']) || !isset($request['params'])) {
            return;
        }
        $method = $request['method'];
        if ($method != 'get_session_key') {
            return;
        }
        // @see https://gitlab.com/r-packages/limonaid/-/issues/2#note_2230524363
        $param = array_values($request['params']);
        if (!isset($param[0])) {
            return;
        }
        $username = $param[0];
        if ($this->api->getUserByName($username)) {
            return;
        }
        $TokenKey = \AuthRemoteToken\models\TokenKey::model()->findByToken($username);
        if ($TokenKey) {
            $this->token = $username;
            if (isset($param[1])) {
                $this->passkey = $param[1];
            }
            $this->isForced = true;
            $this->setRemoteHandler();
            $event->set('run', false);
        }
    }

    /**
     * set the handler to own
     * @return void
     */
    private function setRemoteHandler()
    {
        $oAdminController = new \AdminController('admin/remotecontrol');
        Yii::import('application.helpers.remotecontrol.*');
        Yii::import("AuthRemoteToken.RemoteControlHandler");

        /* LSYii_Controller.php : Sepond time if needed */
        Yii::import("application.helpers.globalsettings");

        /* in Survey_Common_Action */
        Yii::import('application.helpers.viewHelper');
        Yii::import('application.controllers.admin.NotificationController');
        Yii::import('application.libraries.Date_Time_Converter');

        $oHandler = new \RemoteControlHandler($oAdminController);
        App()->loadLibrary('LSjsonRPCServer');
        if (!isset($_SERVER['CONTENT_TYPE'])) {
            $serverContentType = explode(';', $_SERVER['HTTP_CONTENT_TYPE']);
            $_SERVER['CONTENT_TYPE'] = reset($serverContentType);
        }
        LSjsonRPCServer::handle($oHandler);
    }
}
