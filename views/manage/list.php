<div class="container">
    <div class="pagetitle h3">Usages</div>
    <?php if (!empty($settings['replaceDefaultLink']['current'])) : ?>
        <div class="row">
            <div class="col-sm-4">You can use default address</div>
            <div class="col-sm-8">
                <code><?= App()->createAbsoluteUrl(
                    'admin/remotecontrol'
                ); ?></code>
            </div>
        </div>
    <?php else: ?>
    <div class="row">
        <div class="col-sm-4">Url adress for usage of you token keys</div>
        <div class="col-sm-8">
            <code><?= App()->createAbsoluteUrl(
                'plugins/unsecure',
                array(
                    'plugin' => 'AuthRemoteToken'
                )
            ); ?></code>
        </div>
    </div>

        <?php
            $coreUrl = App()->createAbsoluteUrl(
                'admin/remotecontrol'
            );
        ?>
            <p>You need to use this url when you use <a href='https://manual.limesurvey.org/RemoteControl_2_API#get_session_key' target='_blank'>get_session_key</a> to get tour Remote Control API key.
            When you use this url, the default plugin are AuthRemoteToken and not Authdb.</p>
            <p>When you get the session key, you can still use this url or the default URL : <a href='<?= $coreUrl ?>'><?= $coreUrl ?></a>, or another plugin url.</p>
    <?php endif; ?>

    <div class="pagetitle h3">List of token key</div>
    <div class='submit-buttons'>
    <?php
        echo CHtml::link(
            gT('Create new Personal token'),
            App()->createUrl(
                'admin/pluginhelper/sa/fullpagewrapper',
                [
                    'plugin' => 'AuthRemoteToken',
                    'method' => 'actionCreate'
                ]
            ),
            array(
                'class' => 'btn btn-success'
            )
        );
    ?>
    </div>
    <?php
    $this->widget('bootstrap.widgets.TbGridView', array(
        'dataProvider' => $model->search(),
        'columns' => $model->columns,
        'filter' => $model,
        'itemsCssClass' => 'table-condensed',
        'id'            => 'tokenkey-grid',
        //~ 'ajaxUpdate'    => 'tokenkey-grid',
        'ajaxUpdate' => false,
        'htmlOptions' => array('class' => 'grid-view table-responsive'),
    ));
    ?>
</div>
