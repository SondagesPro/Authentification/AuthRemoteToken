<div class="container">
    <div class="pagetitle h3">Edit token key</div>
    <?= CHtml::form(
        App()->createUrl(
            'admin/pluginhelper',
            [
                'sa' => 'fullpagewrapper',
                'plugin' => 'AuthRemoteToken',
                'method' => 'actionSave',
                'token' => $token
            ]
        ),
        'post',
        [
            "id" => "AuthRemoteToken-token",
        ]
    );?>
    <?php
        $this->widget('ext.SettingsWidget.SettingsWidget', array(
            //'id'=>'summary',
            //'title'=>$legend,
            //'prefix' => $pluginClass, This break the label (id!=name)
            'form' => false,
            'formHtmlOptions' => array(
                'class' => 'form-core',
            ),
            'labelWidth' => 4,
            'controlWidth' => 6,
            'settings' => $settings,
        ));
    ?>
    <div class='row'>
      <div class='text-center submit-buttons'>
        <?php
            echo CHtml::htmlButton(
                '<i class="fa fa-check" aria-hidden="true"></i> ' . gT('Save'),
                array(
                    'type' => 'submit',
                    'name' => 'saveAuthRemoteToken',
                    'value' => 'save',
                    'class' => 'btn btn-primary'
                )
            );
            echo " ";
            echo CHtml::htmlButton(
                '<i class="fa fa-check-circle-o " aria-hidden="true"></i> ' . gT('Save and close'),
                array(
                    'type' => 'submit',
                    'name' => 'saveAuthRemoteToken',
                    'value' => 'redirect',
                    'class' => 'btn btn-default'
                )
            );
            echo " ";
            echo CHtml::link(
                gT('Close'),
                App()->createUrl(
                    'admin/pluginhelper',
                    [
                        'sa' => 'fullpagewrapper',
                        'plugin' => 'AuthRemoteToken',
                        'method' => 'actionList'
                    ]
                ),
                array(
                    'class' => 'btn btn-warning'
                )
            );
            echo " ";
            echo CHtml::htmlButton(
                '<i class="fa fa-trash" aria-hidden="true"></i> ' . gT('Delete'),
                array(
                    'type' => 'submit',
                    'name' => 'deleteAuthRemoteToken',
                    'value' => 'delete',
                    'class' => 'btn btn-danger'
                )
            );
            echo " ";
        ?>
      </div>
    </div>
    </form>

</div>
